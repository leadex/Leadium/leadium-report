package org.leadium.report

/**
 * Provider of test steps
 */
object StepProvider {

    private val stepReceiverList = ArrayList<StepReceiver>()

    /**
     * Method registers receivers of log events
     *
     * @param receiver StepReceiver
     */
    fun register(receiver: StepReceiver) {
        stepReceiverList.add(receiver)
    }

    /**
     * Method removes given receiver of log events
     *
     * @param receiver StepReceiver
     */
    fun remove(receiver: StepReceiver) {
        stepReceiverList.remove(receiver)
    }

    /**
     * Method removes all receivers of log events
     */
    fun removeAllListeners() {
        stepReceiverList.clear()
    }

    /**
     * Before advice
     */
    fun before(uuid: String, name: String) =
        stepReceiverList.forEach { it.before(uuid, name) }

    /**
     * After returning advice
     */
    fun afterReturning(uuid: String) =
        stepReceiverList.forEach { it.afterReturning(uuid) }

    /**
     * After throwing advice
     */
    fun afterThrowing(uuid: String, e: Throwable) =
        stepReceiverList.forEach { it.afterThrowing(uuid, e) }

    /**
     * After advice
     */
    fun after(uuid: String) = stepReceiverList.forEach { it.after(uuid) }
}