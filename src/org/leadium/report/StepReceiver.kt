package org.leadium.report

/**
 * Receiver of test steps
 */
interface StepReceiver {

    /**
     * Before advice
     */
    fun before(uuid: String, name: String)

    /**
     * After returning advice
     */
    fun afterReturning(uuid: String)

    /**
     * After throwing advice
     */
    fun afterThrowing(uuid: String, e: Throwable)

    /**
     * After advice
     */
    fun after(uuid: String)
}