package org.leadium.report

import java.io.InputStream

interface AttachmentReceiver {

    fun addAttachment(name: String, type: String, fileExtension: String, body: ByteArray)

    fun addAttachment(name: String, type: String, fileExtension: String, stream: InputStream)
}