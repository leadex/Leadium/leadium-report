package org.leadium.report

import java.io.InputStream

/**
 * Provider of attachments
 */
object AttachmentProvider {

    private val attachmentReceiverList = ArrayList<AttachmentReceiver>()

    /**
     * Method registers receivers of attachments
     *
     * @param receiver StepReceiver
     */
    fun register(receiver: AttachmentReceiver) {
        attachmentReceiverList.add(receiver)
    }

    /**
     * Method removes given receiver of attachments
     *
     * @param receiver StepReceiver
     */
    fun remove(receiver: AttachmentReceiver) {
        attachmentReceiverList.remove(receiver)
    }

    /**
     * Method removes all receivers of attachments
     */
    fun removeAllListeners() {
        attachmentReceiverList.clear()
    }

    //todo doc
    fun addAttachment(name: String, type: String, fileExtension: String, body: ByteArray) {
        attachmentReceiverList.forEach { it.addAttachment(name, type, fileExtension, body) }
    }

    fun addAttachment(name: String, type: String, fileExtension: String, stream: InputStream) {
        attachmentReceiverList.forEach { it.addAttachment(name, type, fileExtension, stream) }
    }
}