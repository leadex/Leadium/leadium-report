package org.leadium.report

import org.leadium.report.AttachmentProvider.addAttachment
import java.io.File

interface AttachmentSyntax {

    fun attachTxt(name: String, body: String) {
        addAttachment(name, "text/plain", "txt", body.toByteArray())
    }

    fun attachJson(name: String, request: String) {
        addAttachment(name, "text/json", "json", request.toByteArray())
    }

    fun attachRequest(url: String, request: String) {
        addAttachment("Request: $url", "text/json", "json", request.toByteArray())
    }

    fun attachRequest(url: String, request: File) {
        addAttachment("Request: $url", "text/json", "json", request.inputStream())
    }

    fun attachPng(name: String, outputFile: File) {
        addAttachment(name, "image/png", "png", outputFile.inputStream())
    }

    fun attachHtml(name: String, body: String) {
        addAttachment(name, "text/html", ".html", body.toByteArray())
    }

    fun attachVideo(file: File) {
        addAttachment("Video", "video/mp4", ".mp4", file.inputStream())
    }
}