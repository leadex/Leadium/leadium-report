package org.leadium.report

import org.leadium.core.utils.replaceCamelCaseWithSpaces
import java.util.*

interface TestStep {

    /**
     * Method provides callbacks for report builders with given name and sub-steps.
     * @param title String
     * @param steps () -> S
     */
    fun <R> step(name: String, steps: () -> R): R {
        val target: R
        val uuid = UUID.randomUUID().toString()
        StepProvider.before(uuid, name)
        try {
            target = steps.invoke()
            StepProvider.afterReturning(uuid)
        } catch (e: Throwable) {
            StepProvider.afterThrowing(uuid, e)
            throw e
        } finally {
            StepProvider.after(uuid)
        }
        return target
    }
}

operator fun <S : TestStep, R> S.minus(block: S.(it: String) -> R): R {
    val name = this.javaClass.simpleName.replaceCamelCaseWithSpaces()
    val target: R
    StepProvider.before(name, name)
    try {
        target = block.invoke(this, name)
        StepProvider.afterReturning(name)
    } catch (e: Throwable) {
        StepProvider.afterThrowing(name, e)
        throw e
    } finally {
        StepProvider.after(name)
    }
    return target
}

operator fun <S : CharSequence, R> S.minus(block: S.(it: String) -> R): R {
    val target: R
    StepProvider.before(this.toString(), this.toString())
    try {
        target = block.invoke(this, this.toString())
        StepProvider.afterReturning(this.toString())
    } catch (e: Throwable) {
        StepProvider.afterThrowing(this.toString(), e)
        throw e
    } finally {
        StepProvider.after(this.toString())
    }
    return target
}